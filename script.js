const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');


password.onkeyup = function() {
    var lowerCaseLetters = /[a-z]/g;
    var upperCaseLetters = /[A-Z]/g;
    var numbers = /[0-9]/g;
    if (password.value.match(lowerCaseLetters) && password.value.match(upperCaseLetters) && password.value.match(numbers) && password.value.length >= 8) {
        setSuccessFor(password);

    } else {
        setErrorFor(password, 'minium 8 chars, contain at least one upperCaseLetters,lowerCaseLetters and a digit')
    }

}

password2.onkeyup = function() {
    if (password.value == password2.value) {
        setSuccessFor(password2);
    } else {
        setErrorFor(password2, 'password not matched')
    }
}

const hide = document.getElementById('togglePassword')
hide.addEventListener('click', function(e) {

    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);

    this.classList.toggle('fa-eye-slash');
});
const hide2 = document.getElementById('togglePassword2')
hide2.addEventListener('click', function(e) {

    const type = password2.getAttribute('type') === 'password' ? 'text' : 'password';
    password2.setAttribute('type', type);

    this.classList.toggle('fa-eye-slash');
});

form.addEventListener('submit', (e) => {
    e.preventDefault();
    check();
})

username.onkeyup = function() {
    if (username.value === '') {
        setErrorFor(username, 'invalid username');
    } else {
        setSuccessFor(username);
    }
}


const check = () => {
    const emailValue = email.value.trim();
    const userValue = username.value.trim();
    const passwordValue = password.value.trim();
    const password2Value = password2.value.trim();
    if (emailValue === '') {
        setErrorFor(email, 'invalid email');
    } else if (!isEmail(emailValue)) {
        setErrorFor(email, 'invalid email');
    } else {
        setSuccessFor(email);
    }
    console.log(userValue);
    if (userValue === '') {
        setErrorFor(username, 'username can\'t be empty');
    } else {
        setSuccessFor(username);
    }

    if (passwordValue === '') {
        setErrorFor(password, 'invalid password');

    } else if (password.value !== password2.value) {
        setErrorFor(password, 'password not matched');
    } else {
        // setSuccessFor(password2);

    }

    if (password2Value === '') {
        setErrorFor(password2, 'invalid password');
    } else {
        // setSuccessFor(password2);
    }



}

function setErrorFor(input, message) {
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');
    formControl.className = 'form-control error';
    small.innerText = message;
}

function setSuccessFor(input) {
    const formControl = input.parentElement;
    formControl.className = 'form-control success';
}

function isEmail(email) {
    return /^([a-z\d\.-]+)@([a-z]+)(\.)([a-z]{2,4})$/.test(email);
}